namespace Base2art.Validation.Features
{
    using System;
    using System.ComponentModel;

    internal class Person
    {
        public Guid PersonId { get; set; }
        public string Name { get; set; }
        public ListSortDirection Direction { get; private set; }

        public DateTime Age;
        
        public int AgeInYears;

        public string GetName()
        {
            return this.Name;
        }
    }
}