namespace Base2art.Validation.Features
{
    using System;
    using Xunit;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using Base2art.Validation;
    using FluentAssertions;

//    using static Base2art.Validation.V1.Validators;

    public class UnitTest1
    {
        [Fact]
        public void ShouldGetExpression()
        {
            var expr = GetExpression(new Person(), x => x.PersonId);
            expr.GetPropertyName().Should().Be("PersonId");

            var expr1 = GetExpression(new Person(), x => x.Age);
            expr1.GetPropertyName().Should().Be("Age");

            var expr2 = GetExpression(new Person(), x => x.GetName());
            expr2.GetPropertyName().Should().Be("GetName");
        }

//        [Fact]
//        public async void ShouldValidate()
//        {
//            var person = new Person { };
//            Func<Task> func = async () => await person.Must().NotBeNull()
//                                                      .And().Object(x => x.Name).Must().NotBeNull();
//
//            await Assert.ThrowsAsync<ArgumentNullException>(func);
//
//            func = async () => await person.Must().NotBeNull()
//                                           .And().Object(x => x.GetName()).Must().NotBeNull();
//
//            await Assert.ThrowsAsync<ArgumentNullException>(func);
//
//            person.Name = "sdf";
//
//            await person.Must().NotBeNull()
//                        .And().Object(x => x.Name).Must().NotBeNull();
//
//            await person.Must().NotBeNull()
//                        .And().Object(x => x.GetName()).Must().NotBeNull();
//
//            person.Name = "";
//
//            func = async () => await person.Must().NotBeNull()
//                                           .And().Object(x => x.Name).Must().NotBeNull();
//
//            await Assert.ThrowsAsync<ArgumentNullException>(func);
//
//            func = async () => await person.Must().NotBeNull()
//                                           .And().Object(x => x.GetName()).Must().NotBeNull();
//
//            await Assert.ThrowsAsync<ArgumentNullException>(func);
//
//            func = async () => await person.Must().NotBeNull()
//                                           .And().Value(x => x.PersonId).Must().NotBe(MatchComparison.EqualTo, Guid.Empty);
//
//            await Assert.ThrowsAsync<ArgumentNullException>(func);
//
//            person.PersonId = Guid.Empty;
//            func = async () => await person.Must().NotBeNull()
//                                           .And().Value(x => x.PersonId).Must().NotBe(MatchComparison.EqualTo, Guid.Empty);
//
//            await Assert.ThrowsAsync<ArgumentNullException>(func);
//
//            person.PersonId = Guid.NewGuid();
//            await person.Must().NotBeNull()
//                        .And().Value(x => x.PersonId).Must().NotBe(MatchComparison.EqualTo, Guid.Empty);
//
//////////////
//            func = async () => await person.Must().NotBeNull()
//                                           .And().Value(x => x.Age).Must().NotBe(MatchComparison.EqualTo, DateTime.MinValue);
//
//            await Assert.ThrowsAsync<ArgumentNullException>(func);
//
//            person.Age = DateTime.MinValue;
//            func = async () => await person.Must().NotBeNull()
//                                           .And().Value(x => x.Age).Must().NotBe(MatchComparison.EqualTo, DateTime.MinValue);
//
//            await Assert.ThrowsAsync<ArgumentNullException>(func);
//
//            person.Age = DateTime.Now;
//            await person.Must().NotBeNull()
//                        .And().Value(x => x.Age).Must().NotBe(MatchComparison.EqualTo, DateTime.MinValue);
//
////            new Action(() => person.Must().HaveValue(x => x.Age)).Should().ThrowExactly<ArgumentNullException>();
////            person.Age = DateTime.MinValue;
////            new Action(() => person.Must().HaveValue(x => x.Age)).Should().ThrowExactly<ArgumentNullException>();
////            person.Age = DateTime.Now;
////            new Action(() => person.Must().HaveValue(x => x.Age)).Should().NotThrow();
//        }
////
////        public void ShouldValidate2()
////        {
////            new Person().Must().NotBeNull();
////            new Person().Must().NotBeNull().And().HaveValue(x => x.Property);
////            new Action(() => person.Must().HaveValue(x => x.PersonId)).Should().ThrowExactly<ArgumentNullException>();
////        }

        private Expression GetExpression<T, TResult>(T person, Expression<Func<T, TResult>> func)
        {
            return func;
        }
    }
}