namespace Base2art.Validation.Features
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Base2art.Validation;
    using FluentAssertions;
    using Xunit;

    public class ValidateFeature
    {
        [Fact]
        public async void AssertItemPassing()
        {
            var person = new Person();
            await person.Validate()
                        .With<ArgumentNullException>(x => x != null)
                        .With<ArgumentNullException>(async x => x != null);
        }

        [Fact]
        public async void AssertItemNullItem()
        {
            Person person = null;

            Func<Task> lookup = async () => await Validation.Validate(() => person)
                                                            .With<ArgumentNullException>(x => x != null)
                                                            .With<ArgumentNullException>(async x => x != null);

            await lookup.Should().ThrowAsync<ArgumentNullException>();
            //.And.ParamName.Should().Be("person");
        }

        [Fact]
        public async void AssertItem()
        {
            var person = new Person();
            Func<Task> lookup = async () => await person.Validate()
                                                        .With<ArgumentNullException>(x => x != null)
                                                        .With<ArgumentNullException>(async x => x != null)
                                                        .WithMember(
                                                                    x => x.Age,
                                                                    age => age.Validate()
                                                                              .With<ArgumentOutOfRangeException>(x => x.Year >= DateTime.UtcNow.Year
                                                                                                                      - 18)
                                                                              .With<ArgumentOutOfRangeException>(async x =>
                                                                                                                     x.Year >= DateTime.UtcNow.Year
                                                                                                                     - 18));

            await lookup.Should().ThrowAsync<ArgumentOutOfRangeException>();
        }

        [Theory]
        [InlineData(true, true)]
        [InlineData(false, false)]
        public async void AssertItem1(bool passNull, bool expectException)
        {
            var person = passNull ? null : new Person();
            Func<Task> lookup = async () => await person.Validate()
                                                        .IsNotNull();

            if (expectException)
            {
                await lookup.Should().ThrowAsync<ArgumentNullException>();
            }
            else
            {
                await lookup();
            }
        }

        [Theory]
        [InlineData(18, 99, 17, true)]
        [InlineData(18, 99, 18, false)]
        [InlineData(18, 99, 19, false)]
        [InlineData(18, 99, 98, false)]
        [InlineData(18, 99, 99, false)]
        [InlineData(18, 99, 100, true)]
        public async void AssertBetweenInclusive(int start, int end, int value, bool expectedException)
        {
            var person = new Person();
            person.AgeInYears = value;
            Func<Task> lookup = async () => await person.Validate()
                                                        .IsNotNull()
                                                        .WithMember(x => x.AgeInYears,
                                                                    age => age.Validate().IsBetweenInclusive(start, end));

            if (expectedException)
            {
                await lookup.Should().ThrowAsync<ArgumentOutOfRangeException>();
            }
            else
            {
                await lookup();
            }
        }

        [Theory]
        [InlineData(18, 99, 17, true)]
        [InlineData(18, 99, 18, true)]
        [InlineData(18, 99, 19, false)]
        [InlineData(18, 99, 98, false)]
        [InlineData(18, 99, 99, true)]
        [InlineData(18, 99, 100, true)]
        public async void AssertBetweenExclusive(int start, int end, int value, bool expectedException)
        {
            var person = new Person();
            person.AgeInYears = value;
            Func<Task> lookup = async () => await person.Validate()
                                                        .IsNotNull()
                                                        .WithMember(x => x.AgeInYears,
                                                                    age => age.Validate().IsBetweenExclusive(start, end));

            if (expectedException)
            {
                await lookup.Should().ThrowAsync<ArgumentOutOfRangeException>();
            }
            else
            {
                await lookup();
            }
        }

        [Theory]
        [InlineData(18, 17, true)]
        [InlineData(18, 18, true)]
        [InlineData(18, 19, false)]
        public async void AssertGreaterThan(int start, int value, bool expectedException)
        {
            var person = new Person();
            person.AgeInYears = value;
            Func<Task> lookup = async () => await person.Validate()
                                                        .IsNotNull()
                                                        .WithMember(x => x.AgeInYears,
                                                                    age => age.Validate().IsGreaterThan(start));

            if (expectedException)
            {
                await lookup.Should().ThrowAsync<ArgumentOutOfRangeException>();
            }
            else
            {
                await lookup();
            }
        }

        [Theory]
        [InlineData(18, 17, true)]
        [InlineData(18, 18, false)]
        [InlineData(18, 19, false)]
        public async void AssertGreaterThanOrEqualTo(int start, int value, bool expectedException)
        {
            var person = new Person();
            person.AgeInYears = value;
            Func<Task> lookup = async () => await person.Validate()
                                                        .IsNotNull()
                                                        .WithMember(x => x.AgeInYears,
                                                                    age => age.Validate().IsGreaterThanOrEqualTo(start));

            if (expectedException)
            {
                await lookup.Should().ThrowAsync<ArgumentOutOfRangeException>();
            }
            else
            {
                await lookup();
            }
        }

        [Theory]
        [InlineData(18, 17, false)]
        [InlineData(18, 18, true)]
        [InlineData(18, 19, true)]
        public async void AssertLessThan(int start, int value, bool expectedException)
        {
            var person = new Person();
            person.AgeInYears = value;
            Func<Task> lookup = async () => await person.Validate()
                                                        .IsNotNull()
                                                        .WithMember(x => x.AgeInYears,
                                                                    age => age.Validate().IsLessThan(start));

            if (expectedException)
            {
                await lookup.Should().ThrowAsync<ArgumentOutOfRangeException>();
            }
            else
            {
                await lookup();
            }
        }

        [Theory]
        [InlineData(18, 17, false)]
        [InlineData(18, 18, false)]
        [InlineData(18, 19, true)]
        public async void AssertLessThanOrEqualTo(int start, int value, bool expectedException)
        {
            var person = new Person();
            person.AgeInYears = value;
            Func<Task> lookup = async () => await person.Validate()
                                                        .IsNotNull()
                                                        .WithMember(x => x.AgeInYears,
                                                                    age => age.Validate().IsLessThanOrEqualTo(start));

            if (expectedException)
            {
                await lookup.Should().ThrowAsync<ArgumentOutOfRangeException>();
            }
            else
            {
                await lookup();
            }
        }

        [Theory]
        [InlineData(null, true)]
        [InlineData("", true)]
        [InlineData(" ", true)]
        [InlineData("\t", true)]
        [InlineData("Test", false)]
        public async void AssertIsNotNullOrEmpty(string value, bool expectedException)
        {
            var person = new Person();
            person.Name = value;
            Func<Task> lookup = async () => await person.Validate()
                                                        .IsNotNull()
                                                        .WithMember(x => x.Name,
                                                                    age => age.Validate().IsNotNullOrWhiteSpace());

            if (expectedException)
            {
                await lookup.Should().ThrowAsync<ArgumentNullException>();
            }
            else
            {
                await lookup();
            }
        }

        [Theory]
        [InlineData(null, null, false)]
        [InlineData("", null, true)]
        [InlineData(null, "", true)]
        [InlineData("SjY", "MjY", true)]
        [InlineData("SjY", "SjY", false)]
        [InlineData("SjY", "SJY", true)]
        public async void AssertIsEqualTo1(string value1, string value2, bool expectedException)
        {
            var person = new Person();
            person.Name = value1;
            Func<Task> lookup = async () => await person.Validate()
                                                        .IsNotNull()
                                                        .WithMember(x => x.Name,
                                                                    name => name.Validate().IsEqualTo(value2));

            if (expectedException)
            {
                await lookup.Should().ThrowAsync<ArgumentOutOfRangeException>();
            }
            else
            {
                await lookup();
            }
        }

        [Theory]
        [InlineData(null, null, false)]
        [InlineData("", null, true)]
        [InlineData(null, "", true)]
        [InlineData("SjY", "MjY", true)]
        [InlineData("SjY", "SjY", false)]
        [InlineData("SjY", "SJY", false)]
        public async void AssertIsEqualTo2(string value1, string value2, bool expectedException)
        {
            var person = new Person();
            person.Name = value1;
            Func<Task> lookup = async () => await person.Validate()
                                                        .IsNotNull()
                                                        .WithMember(x => x.Name,
                                                                    name => name.Validate().IsEqualTo(value2, StringComparer.OrdinalIgnoreCase));

            if (expectedException)
            {
                await lookup.Should().ThrowAsync<ArgumentOutOfRangeException>();
            }
            else
            {
                await lookup();
            }
        }

        [Theory]
        [InlineData(2, false)]
        [InlineData(3, true)]
        public async void AssertArrayLength(int length, bool expectedException)
        {
            var items = new string[] {"a", "b"};

            Func<Task> lookup = async () => await items.Validate()
                                                       .IsNotNull()
                                                       .HasLength(length);

            if (expectedException)
            {
                await lookup.Should().ThrowAsync<ArgumentOutOfRangeException>();
            }
            else
            {
                await lookup();
            }
        }

        [Theory]
        [InlineData(1, false)]
        [InlineData(0, true)]
        public async void AssertArrayIsNotEmpty(int length, bool expectedException)
        {
            List<string> items = new string[length].ToList();

            Func<Task> lookup = async () => await items.Validate()
                                                       .IsNotNull()
                                                       .IsNotEmpty();

            if (expectedException)
            {
                await lookup.Should().ThrowAsync<ArgumentOutOfRangeException>();
            }
            else
            {
                await lookup();
            }
        }

        [Theory]
        [InlineData(1, true)]
        [InlineData(0, false)]
        public async void AssertArrayIsEmpty(int length, bool expectedException)
        {
            List<string> items = new string[length].ToList();

            Func<Task> lookup = async () => await items.Validate()
                                                       .IsNotNull()
                                                       .IsEmpty();

            if (expectedException)
            {
                await lookup.Should().ThrowAsync<ArgumentOutOfRangeException>();
            }
            else
            {
                await lookup();
            }
        }
    }
}