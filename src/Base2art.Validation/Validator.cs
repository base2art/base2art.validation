namespace Base2art.Validation
{
    using System;
    using System.Threading.Tasks;

    public class Validator<T>
    {
        private readonly T data;
        private readonly ValidatingContainer<T> container = new ValidatingContainer<T>();

        public Validator(T data)
        {
            this.data = data;
        }

        public ValidatorAwaiter<T> GetAwaiter()
        {
            return new ValidatorAwaiter<T>(this.container);
        }

        public Validator<T> With<TException>(Func<T, Task<bool>> func)
            where TException : Exception, new()
        {
            this.container.FailIf<TException>(() => func(this.data));
            return this;
        }

        public Validator<T> With<TException>(Func<T, bool> func)
            where TException : Exception, new()
        {
            return this.With<TException>(x => Task.FromResult(func(x)));
        }

        public Validator<T> WithMember<TOther>(Func<T, TOther> lookup, Func<TOther, Validator<TOther>> validation)
        {
            // <TException>
            this.container.FailIf<AggregateException>(async () =>
            {
                var otherData = lookup(this.data);

                var result = await validation(otherData);
                if (result > -1)
                {
                    return false;
                }

                return true;
            });

            return this;
        }
    }
}