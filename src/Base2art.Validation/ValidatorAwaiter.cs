namespace Base2art.Validation
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Threading.Tasks;

    public class ValidatorAwaiter<T> : INotifyCompletion
    {
        private readonly ValidatingContainer<T> container;
        private readonly TaskAwaiter<int> task;

        public ValidatorAwaiter(ValidatingContainer<T> container)
        {
            this.container = container;
            this.task = this.container.Validate().GetAwaiter();
        }

        public void OnCompleted(Action continuation)
        {
            this.task.OnCompleted(continuation);
        }

        public bool IsCompleted => this.task.IsCompleted;

        public int GetResult()
        {
            var result = this.task.GetResult();

            if (result >= 0)
            {
                throw (Exception) Activator.CreateInstance(this.container.GetException(result));
            }

            return result;
        }
    }
}