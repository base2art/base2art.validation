namespace Base2art.Validation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;

    public static class Validators
    {
        public static Validator<T> IsNull<T>(this Validator<T> @value)
            where T : class
        {
            return @value.With<ArgumentNullException>(x => x == null);
        }

        public static Validator<T> IsNotNull<T>(this Validator<T> @value)
            where T : class
        {
            return @value.With<ArgumentNullException>(x => x != null);
        }

        public static Validator<T> IsDefault<T>(this Validator<T> @value)
            where T : IComparable<T>
        {
            return @value.With<ArgumentNullException>(IsDefault);
        }

        public static Validator<T> IsNotDefault<T>(this Validator<T> @value)
            where T : IComparable<T>
        {
            return @value.With<ArgumentNullException>(x => !IsDefault(x));
        }

        public static Validator<string> IsNotNullOrWhiteSpace(this Validator<string> @value)
        {
            return @value.With<ArgumentNullException>(x => !string.IsNullOrWhiteSpace(x));
        }

        public static Validator<T> IsBetweenInclusive<T>(this Validator<T> @value, T start, T end)
            where T : struct, IComparable<T>
        {
            return value.With<ArgumentOutOfRangeException>(x => start.CompareTo(x) <= 0)
                        .With<ArgumentOutOfRangeException>(x => end.CompareTo(x) >= 0);
        }

        public static Validator<T> IsBetweenExclusive<T>(this Validator<T> @value, T start, T end)
            where T : struct, IComparable<T>
        {
            return value.With<ArgumentOutOfRangeException>(x => start.CompareTo(x) < 0)
                        .With<ArgumentOutOfRangeException>(x => end.CompareTo(x) > 0);
        }

        public static Validator<T> IsGreaterThan<T>(this Validator<T> @value, T start)
            where T : struct, IComparable<T>
        {
            return value.With<ArgumentOutOfRangeException>(x => start.CompareTo(x) < 0);
        }

        public static Validator<T> IsGreaterThanOrEqualTo<T>(this Validator<T> @value, T start)
            where T : struct, IComparable<T>
        {
            return value.With<ArgumentOutOfRangeException>(x => start.CompareTo(x) <= 0);
        }

        public static Validator<T> IsLessThan<T>(this Validator<T> @value, T start)
            where T : struct, IComparable<T>
        {
            return value.With<ArgumentOutOfRangeException>(x => start.CompareTo(x) > 0);
        }

        public static Validator<T> IsLessThanOrEqualTo<T>(this Validator<T> @value, T start)
            where T : struct, IComparable<T>
        {
            return value.With<ArgumentOutOfRangeException>(x => start.CompareTo(x) >= 0);
        }

        public static Validator<T> IsEqualTo<T>(this Validator<T> @validator, T value)
            where T : IComparable<T>
        {
            return validator.With<ArgumentOutOfRangeException>(x =>
            {
                if (x == null && value == null)
                {
                    return true;
                }

                if (x == null ^ value == null)
                {
                    return false;
                }

                return value.CompareTo(x) == 0;
            });
        }

        public static Validator<T> IsEqualTo<T>(this Validator<T> @validator, T value, IEqualityComparer<T> comp)
        {
            if (comp == null)
            {
                throw new ArgumentNullException(nameof(comp));
            }

            return validator.With<ArgumentOutOfRangeException>(x => comp.Equals(x, value));
        }

        public static Validator<T[]> HasLength<T>(this Validator<T[]> @validator, int length)
        {
            return validator.With<ArgumentOutOfRangeException>(x => x.Length == length);
        }

        public static Validator<T> IsNotEmpty<T>(this Validator<T> @validator)
            where T : System.Collections.Generic.IEnumerable<object>
        {
            return validator.With<ArgumentOutOfRangeException>(x => x.Any());
        }

        public static Validator<T> IsEmpty<T>(this Validator<T> @validator)
            where T : System.Collections.Generic.IEnumerable<object>
        {
            return validator.With<ArgumentOutOfRangeException>(x => !x.Any());
        }

        private static bool IsDefault<T>(this T @value)
            where T : IComparable<T>
        {
            if (value == null)
            {
                return true;
            }

            return value.CompareTo(default) == 0;
        }
    }
}