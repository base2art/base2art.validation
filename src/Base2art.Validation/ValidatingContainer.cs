namespace Base2art.Validation
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class ValidatingContainer<T>
    {
        private readonly List<Tuple<Type, Func<Task<bool>>>> lookups = new List<Tuple<Type, Func<Task<bool>>>>();

        public async Task<int> Validate()
        {
            for (int i = 0; i < this.lookups.Count; i++)
            {
                var item = this.lookups[i];

                var result = await item.Item2();

                if (!result)
                {
                    return i;
                }
            }

            return -1;
        }

        public void FailIf<TException>(Func<Task<bool>> func)
            where TException : Exception, new()
        {
            this.lookups.Add(Tuple.Create(typeof(TException), func));
        }

        public Type GetException(int result)
        {
            return this.lookups[result].Item1;
        }
    }
}