namespace Base2art.Validation
{
    using System.Linq.Expressions;

    public static class Expresser
    {
        public static string GetPropertyName(this Expression lambda)
            => lambda is LambdaExpression propertyLambda ? Map(propertyLambda) : "";

        private static string Map(LambdaExpression propertyLambda)
        {
            switch (propertyLambda.Body)
            {
                case MemberExpression member:
                    return member.Member.Name;
                case MethodCallExpression call:
                    return call.Method.Name;
                default:
                    return "";
            }
        }
    }
}

//                    throw new ArgumentException($"Expression '{propertyLambda.ToString()}' refers to a method, not a property.");
//                PropertyInfo propInfo = member.Member as PropertyInfo;
//                if (propInfo != null)
//                {
//                    return propInfo.Name;
////                    throw new ArgumentException($"Expression '{propertyLambda.ToString()}' refers to a field, not a property.");
//                }

//                if (type != propInfo.ReflectedType && !type.IsSubclassOf(propInfo.ReflectedType))
//                {
//                    throw new ArgumentException($"Expression '{propertyLambda.ToString()}' refers to a property that is not from type {type}.");
//                }
//
//                return null;