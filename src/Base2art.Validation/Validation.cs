namespace Base2art.Validation
{
    using System;
    using System.Linq.Expressions;

    public static class Validation
    {
        public static Validator<T> Validate<T>(this Expression<Func<T>> @value)
        {
            return value.Compile()().Validate();
        }

        public static Validator<T> Validate<T>(this T @value)
        {
            return new Validator<T>(value);
        }
    }
}